//
//  NewsGenTests.swift
//  NewsGenTests
//
//  Created by Syamsul Falah on 08/08/23.
//

import XCTest
@testable import NewsGen

final class NewsGenTests: XCTestCase {
    
    var presenter: NewsPresenter!
    var view: MockNewsViewController!
    var interactor: MockNewsInteractor!
    var router: MockNewsRouter!

    override func setUp() {
        super.setUp()
        view = .init()
        interactor = .init()
        router = .init()
        presenter = .init(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        router.viewController = view
        interactor.presenter = presenter
    }
    
    override func tearDown() {
        view = nil
        interactor = nil
        router = nil
        presenter = nil
    }
    
    func testFetchHeadlineDataSuccess() {
        presenter.fetchHeadlineData(category: .general, page: 1, keyword: nil, language: .indonesia)
        
        let articlesCountExpected = 20
        let firstArticleTitleExpected = "HIGHLIGHTS: Club América vs Nashville"
        
        XCTAssertTrue(interactor.isFetchHeadlineDataCalled)
        XCTAssertEqual(interactor.articles.count, articlesCountExpected)
        XCTAssertTrue(view.isCalledDisplayHeadline)
        XCTAssertEqual(view.firstArticleTitle, firstArticleTitleExpected)
    }
    
    func testFetchHeadlineWithEmptyData() {
        interactor.isArticlesEmpty = true
        presenter.fetchHeadlineData(category: .general, page: 1, keyword: "yyyyy", language: .indonesia)
        
        let articlesCountExpected = 0
        
        XCTAssertTrue(interactor.isFetchHeadlineDataCalled)
        XCTAssertEqual(interactor.articles.count, articlesCountExpected)
        XCTAssertTrue(view.isCalledDisplayHeadline)
        XCTAssertEqual(view.firstArticleTitle, "")
    }
    
    func testFetchHeadlineError() {
        interactor.isError = true
        presenter.fetchHeadlineData(category: .general, page: 1, keyword: nil, language: .indonesia)
        
        XCTAssertTrue(interactor.isFetchHeadlineDataCalled)
        XCTAssertEqual(interactor.articles.count, 0)
        XCTAssertTrue(view.isCalledDisplayError)
        XCTAssertEqual(view.errorMessage, "Something wrong")
    }
}



final class MockNewsViewController: NewsViewControllerProtocol {
    
    var presenter: NewsPresenterProtocol?
    
    var isCalledDisplayHeadline = false
    var isCalledDisplayError = false
    
    var firstArticleTitle = ""
    var errorMessage = ""
    
    func displayHeadline(articles: [Article], hasMoreArticles: Bool) {
        firstArticleTitle = articles.first?.title ?? ""
        isCalledDisplayHeadline = true
    }
    
    func displayError(error: Error) {
        isCalledDisplayError = true
        
        let myError = error as! MyError
        switch myError {
        case .badConnection:
            errorMessage = "Bad Connection"
        case .genericError(let message):
            errorMessage = message
        }
    }
}


final class MockNewsInteractor: NewsInteractorProtocol {
    
    var presenter: NewsPresenterProtocol?
    
    var articles: [Article] = []
    var isFetchHeadlineDataCalled = false
    var isArticlesEmpty = false
    var isError = false
    
    func fetchHeadlineData(category: HeadlineCategory, page: Int, keyword: String?, language: Languanges) {
        isFetchHeadlineDataCalled = true
        
        if isError {
            presenter?.presentError(error: MyError.genericError(message: "Something wrong"))
        } else {
            if !isArticlesEmpty {
                articles = Article.previewData
            }
            presenter?.presentHeadline(articles: articles, hasMoreArticles: true)
        }
        
    }
}

final class MockNewsRouter: NewsRouterProtocol {
    
    var viewController: NewsViewControllerProtocol?
    
    var isNavigateToNewsDetail = false
    
    func navigateToNewsDetail(url: URL) {
        isNavigateToNewsDetail = true
    }
}
