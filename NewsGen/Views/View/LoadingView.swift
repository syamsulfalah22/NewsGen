//
//  LoadingView.swift
//  NewsGen
//
//  Created by Syamsul Falah on 09/08/23.
//

import Lottie
import UIKit

class LoadingView: UIView {
    
    private let animationView: LottieAnimationView = {
        let view = LottieAnimationView(name: "loading")
        view.translatesAutoresizingMaskIntoConstraints = false
        view.loopMode = .loop
        view.animationSpeed = 1.0
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAnimationView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAnimationView()
    }
    
    private func setupAnimationView() {
        addSubview(animationView)
        
        NSLayoutConstraint.activate([
            animationView.centerXAnchor.constraint(equalTo: centerXAnchor),
            animationView.centerYAnchor.constraint(equalTo: centerYAnchor),
            animationView.heightAnchor.constraint(equalToConstant: 250),
            animationView.widthAnchor.constraint(equalToConstant: 250)
        ])
    }
    
    func play() {
        animationView.play()
    }
    
    func stop() {
        animationView.stop()
    }
}
