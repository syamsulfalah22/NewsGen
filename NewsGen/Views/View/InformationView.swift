//
//  InformationView.swift
//  NewsGen
//
//  Created by Syamsul Falah on 10/08/23.
//

import UIKit

class InformationView: UIView {
    
    private let infoImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.tintColor = .gray
        return view
    }()
    
    private let infoLabel: UILabel = {
       let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        view.font = .preferredFont(forTextStyle: .caption1)
        view.numberOfLines = 2
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupViews()
    }
    
    private func setupViews() {
        addSubview(infoImageView)
        addSubview(infoLabel)
        
        NSLayoutConstraint.activate([
            infoImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            infoImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            infoImageView.widthAnchor.constraint(equalToConstant: 60),
            infoImageView.heightAnchor.constraint(equalToConstant: 60),
            
            infoLabel.topAnchor.constraint(equalTo: infoImageView.bottomAnchor, constant: 10),
            infoLabel.centerXAnchor.constraint(equalTo: infoImageView.centerXAnchor)
        ])
    }
    
    func setupContent(information: InformationCategory) {
        infoImageView.image = information.image
        infoLabel.text = information.description
    }
}
