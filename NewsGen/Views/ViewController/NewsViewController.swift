//
//  ViewController.swift
//  NewsGen
//
//  Created by Syamsul Falah on 08/08/23.
//

import UIKit

protocol NewsViewControllerProtocol: AnyObject {
    func displayHeadline(articles: [Article], hasMoreArticles: Bool)
    func displayError(error: Error)
}

class NewsViewController: UIViewController {
    
    private let tableView: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.estimatedRowHeight = UITableView.automaticDimension
        return view
    }()
    
    private let searchController: UISearchController = {
        let view = UISearchController(searchResultsController: nil)
        view.obscuresBackgroundDuringPresentation = false
        view.searchBar.placeholder = "Search"
        return view
    }()
    
    private lazy var loadingView: LoadingView = {
        let view = LoadingView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    private lazy var informationView: InformationView = {
        let view = InformationView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    private let refreshControl = UIRefreshControl()
    
    private var articles: [Article] = []
    private let idNewsCell = "NewsCell"
    
    var presenter: NewsPresenterProtocol?
    
    private var currentPage = 1
    private var hasMoreArticles = true
    private var categorySelected: HeadlineCategory = .general
    private var languageSelected: Languanges = .indonesia
    private var searchKeyword: String?
    private var isPullRefresh = false
    
    init() {
        super.init(nibName: nil, bundle: nil)
        NewsRouter.createModule(view: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupFilterViews()
        setupConstraint()
        resetFetchData()
        getHeadline()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
        title = categorySelected.titleCategory
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }
    
    private func setupViews() {
        view.backgroundColor = UIColor(red: 239/255.0, green: 239/255.0, blue: 239/255.0, alpha: 1.0)
        view.addSubview(tableView)
        view.addSubview(loadingView)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(NewsTableViewCell.self, forCellReuseIdentifier: idNewsCell)
        tableView.addSubview(refreshControl)
        tableView.addSubview(informationView)
        
        informationView.frame = tableView.bounds
        
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        
        navigationController?.navigationBar.tintColor = .black
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        refreshControl.addTarget(self, action: #selector(refreshNews(_:)), for: .valueChanged)
    }
    
    private func setupConstraint() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            loadingView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
            loadingView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor),
            
            informationView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
            informationView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor, constant: -80),
        ])
    }
    
    private func setupFilterViews() {
        let filterMenu = generateFilterMenu()
        let categoryBarButtonItem = UIBarButtonItem(
            image: UIImage(systemName: "line.3.horizontal.decrease.circle"),
            primaryAction: nil,
            menu: UIMenu(title: "", children: filterMenu)
        )
        
        let languangeMenu = generateLanguangeMenu()
        let languageBarButtonItem = UIBarButtonItem(
            image: UIImage(systemName: "globe"),
            primaryAction: nil,
            menu: UIMenu(title: "", children: languangeMenu)
        )
        
        categoryBarButtonItem.tintColor = .gray
        languageBarButtonItem.tintColor = .gray
        
        navigationItem.rightBarButtonItems = [categoryBarButtonItem, languageBarButtonItem]
    }
    
    private func generateFilterMenu() -> [UIAction] {
        let menus = HeadlineCategory.allCases.map{ [weak self] category in
            let state: UIMenuElement.State = category == self?.categorySelected ? .on : .off
            let image = UIImage.init(systemName: category.filterImage)
            return UIAction(title: category.titleCategory, image: image, state: state) { [weak self] action in
                self?.categorySelected = category
                self?.reloadFilter()
            }
        }
        return menus
    }
    
    private func generateLanguangeMenu() -> [UIAction] {
        let menus = Languanges.allCases.map { [weak self] language in
            let state: UIMenuElement.State = language == self?.languageSelected ? .on : .off
            return UIAction(title: language.titleLanguange, state: state) { [weak self] action in
                self?.languageSelected = language
                self?.reloadFilter()
            }
        }
        return menus
    }
    
    private func reloadFilter() {
        setupFilterViews()
        resetFetchData()
        getHeadline()
    }
    
    private func resetFetchData() {
        title = categorySelected.titleCategory
        currentPage = 1
        hasMoreArticles = true
        articles.removeAll()
        searchKeyword = nil
    }
    
    private func getHeadline() {
        if currentPage == 1 || isPullRefresh {
            isPullRefresh = false
            showLoadingAnimation()
        }
        presenter?.fetchHeadlineData(category: categorySelected, page: currentPage, keyword: searchKeyword, language: languageSelected)
    }
    
    @objc func refreshNews(_ sender: Any) {
        isPullRefresh = true
        searchController.searchBar.text = ""
        resetFetchData()
        getHeadline()
    }
}

extension NewsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        informationView.isHidden = !articles.isEmpty
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: idNewsCell, for: indexPath) as? NewsTableViewCell, articles.count > indexPath.row else {
            return UITableViewCell()
        }
        let currentArticle = articles[indexPath.row]
        cell.setupContent(article: currentArticle)
        return cell
    }
}

extension NewsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.row == articles.count - 3, hasMoreArticles else { return }
        currentPage += 1
        getHeadline()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let url = articles[indexPath.row].articleUrl else { return }
        presenter?.didSelectNews(url: url)
    }
}

extension NewsViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text else { return }
        searchBar.resignFirstResponder()
        searchKeyword = searchText
        articles.removeAll()
        currentPage = 1
        tableView.reloadData()
        getHeadline()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        hideLoadingAnimation()
    }
}

extension NewsViewController: NewsViewControllerProtocol {
    func displayHeadline(articles: [Article], hasMoreArticles: Bool) {
        self.articles = articles
        self.hasMoreArticles = hasMoreArticles
        hideLoadingAnimation()
        refreshControl.endRefreshing()
        
        if articles.isEmpty {
            informationView.setupContent(information: .dataNotFound)
        }
        
        tableView.reloadData()
    }
    
    func displayError(error: Error) {
        informationView.setupContent(information: .someError)
    }
}

extension NewsViewController {
    func showLoadingAnimation() {
        tableView.isHidden = true
        loadingView.isHidden = false
        loadingView.play()
    }
    
    func hideLoadingAnimation() {
        loadingView.isHidden = true
        tableView.isHidden = false
        loadingView.stop()
    }
}
