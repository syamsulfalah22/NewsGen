//
//  NewsTableViewCell.swift
//  NewsGen
//
//  Created by Syamsul Falah on 08/08/23.
//

import UIKit
import Kingfisher

class NewsTableViewCell: UITableViewCell {
        
    private let bannerImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        return view
    }()
    
    private let titleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .preferredFont(forTextStyle: .headline)
        view.numberOfLines = 3
        return view
    }()
    
    private let subtitleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .preferredFont(forTextStyle: .subheadline)
        view.numberOfLines = 2
        return view
    }()
    
    private let captionLabel: UILabel = {
       let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .secondaryLabel
        view.font = .preferredFont(forTextStyle: .caption1)
        view.numberOfLines = 1
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        resetViews()
    }
    
    private func setupViews() {
        selectionStyle = .none
        backgroundColor = .clear
        
        [bannerImageView, titleLabel, subtitleLabel, captionLabel].forEach{ contentView.addSubview($0) }

        NSLayoutConstraint.activate([
            bannerImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            bannerImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            bannerImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            bannerImageView.heightAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 9/16),

            titleLabel.topAnchor.constraint(equalTo: bannerImageView.bottomAnchor, constant: 12),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),

            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8),
            subtitleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            subtitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),

            captionLabel.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor, constant: 8),
            captionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            captionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            captionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -26)
        ])
    }
    
    private func resetViews() {
        bannerImageView.kf.cancelDownloadTask()
        bannerImageView.image = nil
        titleLabel.text = nil
        subtitleLabel.text = nil
        captionLabel.text = nil
    }
    
    func setupContent(article: Article) {
        
        // TODO: Need to handle when bad connection
        
        let options: KingfisherOptionsInfo? = [
            .cacheSerializer(FormatIndicatedCacheSerializer.jpeg),
            .processingQueue(.dispatch(.global(qos: .background))),
            .processor(DownsamplingImageProcessor(size: .init(width: 250, height: 250))),
            .transition(.fade(0.2))
        ]
        
        let imageUrl = article.imageUrl
        bannerImageView.kf.setImage(with: imageUrl, options: options) { [weak self] result in
            switch result{
            case .success(let resultImage):
                self?.bannerImageView.image = resultImage.image
                self?.bannerImageView.contentMode = .scaleAspectFill
            case .failure:
                self?.bannerImageView.image = .init(systemName: "photo.fill")
                self?.bannerImageView.tintColor = .gray
                self?.bannerImageView.contentMode = .scaleAspectFit
            }
        }
        
        titleLabel.text = article.title
        subtitleLabel.text = article.descriptionText
        captionLabel.text = article.captionText
    }
}

