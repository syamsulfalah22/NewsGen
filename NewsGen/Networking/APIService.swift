//
//  APIService.swift
//  NewsGen
//
//  Created by Syamsul Falah on 09/08/23.
//

import Alamofire

typealias ResultNewsHanlder = (Result<NewsAPIResponse, Error>) -> Void

protocol APiServiceProtocol {
    func fetchData(from router: URLRequestConvertible, completion: @escaping ResultNewsHanlder)
}

class APIService: APiServiceProtocol {
    
    static let shared = APIService()
    
    func fetchData(from router: URLRequestConvertible, completion: @escaping ResultNewsHanlder) {
        AF.request(router).responseData { response in
            switch response.result {
            case .success(let data):
                
                let jsonDecoder = JSONDecoder()
                jsonDecoder.dateDecodingStrategy = .iso8601
                
                let apiResponse = try! jsonDecoder.decode(NewsAPIResponse.self, from: data)
                
                completion(.success(apiResponse))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
