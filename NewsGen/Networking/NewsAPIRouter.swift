//
//  NewsAPIRouter.swift
//  NewsGen
//
//  Created by Syamsul Falah on 09/08/23.
//

import Alamofire

enum NewsAPIRouter {
    case getHeadline(category: HeadlineCategory, page: Int, keyword: String?, language: Languanges)
}

extension NewsAPIRouter: URLRequestConvertible {
    
    var baseURL: URL {
        return URL(string: "https://newsapi.org/v2")!
    }
    
    var path: String {
        switch self {
        case .getHeadline:
            return "/top-headlines"
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .getHeadline(let category, let page, let keyword, let language):
            var params: [String: Any] = [
                "category": category.rawValue,
                "page": page,
                "language": language.parameterLanguage
            ]
            
            if let keyword = keyword {
                params["q"] = keyword
            }
            
            return params
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getHeadline:
            return .get
        }
    }
    
    var headers: HTTPHeaders {
        var headers = HTTPHeaders()
        headers["Authorization"] = "0ddd0df9293941b2b0f64850db18cb4e"
        return headers
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = baseURL.appendingPathComponent(path)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.headers = headers
        
        switch self {
        case .getHeadline:
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}
