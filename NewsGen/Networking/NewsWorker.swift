//
//  NewsWorker.swift
//  NewsGen
//
//  Created by Syamsul Falah on 09/08/23.
//

import Alamofire

protocol NewsWorkerProtocol {
    func fetchDataHeadline(from router: URLRequestConvertible, completion: @escaping ResultNewsHanlder)
}

class NewsWorker: NewsWorkerProtocol {
    
    private let apiService: APiServiceProtocol
    
    init(apiService: APiServiceProtocol) {
        self.apiService = apiService
    }
    
    func fetchDataHeadline(from router: URLRequestConvertible, completion: @escaping ResultNewsHanlder) {
        apiService.fetchData(from: router, completion: completion)
    }
}
