//
//  MyError.swift
//  NewsGen
//
//  Created by Syamsul Falah on 10/08/23.
//

import Foundation

enum MyError: Error {
    case badConnection
    case genericError(message: String)
}
