//
//  InformationCategory.swift
//  NewsGen
//
//  Created by Syamsul Falah on 10/08/23.
//

import UIKit

enum InformationCategory {
    case dataNotFound
    case someError
    
    var image: UIImage {
        switch self {
        case .dataNotFound:
            return UIImage(named: "dataNotFound")!
        case .someError:
            return UIImage(named: "someError")!
        }
    }
    
    var description: String {
        switch self {
        case .dataNotFound:
            return "News is not found. \nTry search use other keyword"
        case .someError:
            return "Oops..something wrong"
        }
    }
}
