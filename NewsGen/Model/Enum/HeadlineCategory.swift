//
//  HeadlineCategory.swift
//  NewsGen
//
//  Created by Syamsul Falah on 09/08/23.
//

import Foundation

enum HeadlineCategory: String, CaseIterable {
    case general
    case business
    case entertainment
    case health
    case science
    case sports
    case technology
    
    var parameterCategory: String {
        return self.rawValue
    }
    
    var titleCategory: String {
        switch self {
        case .business, .entertainment, .health, .science, .sports, .technology:
            return parameterCategory.capitalized
        case .general:
            return "Headline"
        }
    }
    
    var filterImage: String {
        switch self {
        case .general:
            return "newspaper"
        case .business:
            return "bag"
        case .entertainment:
            return "gamecontroller"
        case .health:
            return "stethoscope"
        case .science:
            return "sun.max"
        case .sports:
            return "figure.handball"
        case .technology:
            return "network"
        }
    }
}
