//
//  Languanges.swift
//  NewsGen
//
//  Created by Syamsul Falah on 10/08/23.
//

import Foundation

enum Languanges: String, CaseIterable {
    case indonesia
    case english
    
    var parameterLanguage: String {
        switch self {
        case .indonesia:
            return "id"
        case .english:
            return "en"
        }
    }
    
    var titleLanguange: String {
        switch self {
        case .indonesia:
            return "Indonesia"
        case .english:
            return "English"
        }
    }
}
