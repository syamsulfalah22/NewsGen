//
//  NewsAPIResponse.swift
//  NewsGen
//
//  Created by Syamsul Falah on 08/08/23.
//

import Foundation

struct NewsAPIResponse: Decodable {
    let status: String
    let totalResults: Int?
    let articles: [Article]?
    
    let code: String?
    let message: String?
}
