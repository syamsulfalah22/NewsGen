//
//  Article.swift
//  NewsGen
//
//  Created by Syamsul Falah on 08/08/23.
//

import Foundation

fileprivate let relativeDateFormatter = RelativeDateTimeFormatter()

struct Article {
    let source: Source
    
    let title: String
    let url: String
    let publishedAt: Date
    
    let author: String?
    let description: String?
    let urlToImage: String?
    
    var authorText: String {
        author ?? ""
    }
    
    var descriptionText: String {
        description ?? ""
    }
    
    var captionText: String {
        "\(source.name) ・ \(relativeDateFormatter.localizedString(for: publishedAt, relativeTo: Date()))"
    }
    
    var articleUrl: URL? {
        return URL(string: url)
    }
    
    var imageUrl: URL? {
        guard let url = urlToImage else { return nil }
        return URL(string: url)
    }
}

extension Article: Codable {}

extension Article {
    static var previewData: [Article] {
        let previewDataUrl = Bundle.main.url(forResource: "newsDummy", withExtension: "json")!
        let data = try! Data(contentsOf: previewDataUrl)
        
        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .iso8601
        
        let apiResponse = try! jsonDecoder.decode(NewsAPIResponse.self, from: data)
        return apiResponse.articles ?? []
    }
}


struct Source {
    let name: String
}

extension Source: Codable {}
