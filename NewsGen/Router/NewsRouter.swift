//
//  NewsRouter.swift
//  NewsGen
//
//  Created by Syamsul Falah on 09/08/23.
//

import UIKit

protocol NewsRouterProtocol {
    func navigateToNewsDetail(url: URL)
}

class NewsRouter {
    
    weak var viewController: NewsViewController?
    
    static func createModule(view: NewsViewController) {
        let interactor = NewsInteractor()
        let router = NewsRouter()
        let presenter = NewsPresenter(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        router.viewController = view
        interactor.presenter = presenter
    }
}

extension NewsRouter: NewsRouterProtocol {
    func navigateToNewsDetail(url: URL) {
        let newsDetailViewController = NewsDetailViewController(url: url)
        viewController?.navigationController?.pushViewController(newsDetailViewController, animated: true)
    }
}
