//
//  NewsPresenter.swift
//  NewsGen
//
//  Created by Syamsul Falah on 09/08/23.
//

import Foundation

protocol NewsPresenterProtocol: AnyObject {
    func fetchHeadlineData(category: HeadlineCategory, page: Int, keyword: String?, language: Languanges)
    func presentHeadline(articles: [Article], hasMoreArticles: Bool)
    func presentError(error: Error)
    func didSelectNews(url: URL)
}

class NewsPresenter: NewsPresenterProtocol {
    
    weak var view: NewsViewControllerProtocol?
    private let interactor: NewsInteractorProtocol
    let router: NewsRouterProtocol
    
    init(view: NewsViewControllerProtocol, interactor: NewsInteractorProtocol, router: NewsRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func fetchHeadlineData(category: HeadlineCategory, page: Int, keyword: String?, language: Languanges) {
        interactor.fetchHeadlineData(category: category, page: page, keyword: keyword,language: language)
    }
    
    func presentHeadline(articles: [Article], hasMoreArticles: Bool) {
        view?.displayHeadline(articles: articles, hasMoreArticles: hasMoreArticles)
    }
    
    func presentError(error: Error) {
        view?.displayError(error: error)
    }
    
    func didSelectNews(url: URL) {
        router.navigateToNewsDetail(url: url)
    }
}
