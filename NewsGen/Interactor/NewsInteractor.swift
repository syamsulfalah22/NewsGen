//
//  NewsInteractor.swift
//  NewsGen
//
//  Created by Syamsul Falah on 09/08/23.
//

import Foundation

protocol NewsInteractorProtocol {
    func fetchHeadlineData(category: HeadlineCategory, page: Int, keyword: String?, language: Languanges)
}

class NewsInteractor: NewsInteractorProtocol {
    
    weak var presenter: NewsPresenterProtocol?
    private let worker = NewsWorker(apiService: APIService.shared)
    
    private var articles: [Article] = []
    
    func fetchHeadlineData(category: HeadlineCategory, page: Int, keyword: String?, language: Languanges) {
        let request = NewsAPIRouter.getHeadline(category: category, page: page, keyword: keyword, language: language)
        worker.fetchDataHeadline(from: request) { [weak self] result in
            guard let self else { return }
            switch result {
            case .success(let response):
                guard let articlesResult = response.articles else { return }
                if page > 1 {
                    self.articles.append(contentsOf: articlesResult)
                } else {
                    self.articles = articlesResult
                }
                let hasMoreArticles = !articlesResult.isEmpty
                self.presenter?.presentHeadline(articles: self.articles, hasMoreArticles: hasMoreArticles)
            case .failure(let error):
                self.presenter?.presentError(error: error)
            }
        }
    }
}
